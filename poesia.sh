#!/bin/bash

recitar_verso() {
    echo -e "\e[38;5;208m$1\e[0m"    
    sleep 2   
}

versos=(
    "Nem tudo o que reluz é ouro,"
    "Nem todos os que vagueiam estão perdidos;"
    "O velho que é forte não murcha,"
    "Raízes profundas não são atingidas pela geada."
)

for verso in "${versos[@]}"; do
    recitar_verso "$verso"
done

